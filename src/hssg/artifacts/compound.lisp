;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; compound.lisp  Compound artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.(in-package #:hssg.artifact)
(in-package #:hssg.artifact._compound)

(defmethod hssg.artifact:derive-artifact ((artifact hssg.artifact:compound-artifact))
  (with-slots ((artifacts hssg.artifact:artifacts)) artifact
    (make-instance 'hssg.filesystem:compound-instruction
      :instructions (mapcar #'hssg.artifact:derive-artifact artifacts))))

(defun make-compound-artifact (&rest artifacts)
  "Create a new compound artifact, which is a wrapper around the given
  ARTIFACTS. Writing a compound artifact writes all the wrapped artifact in the
  same order that was given as argument."
  (make-instance 'hssg.artifact:compound-artifact :artifacts artifacts))

(defun compound-artifact-push (compound artifact)
  "Push ARTIFACT to the beginning of the list or artifacts of the COMPOUND
  artifact.  This mutates COMPOUND in-place."
  (with-slots ((artifacts hssg.artifact:artifacts))
      compound
    (push artifact artifacts)))
