;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; html.lisp  HTML artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact.html)

(defmethod hssg.artifact:derive-artifact ((artifact hssg.artifact:html-artifact))
  (with-slots ((data hssg.artifact::data)
               (template hssg.artifact::template)
               (output hssg.artifact::output))
      artifact
    (let* ((plump:*tag-dispatchers* plump:*xml-tags*)
           (contents (plump:serialize
                       (sexp->plump-tree (cdr (assoc :content (funcall template data))))
                       nil)))
      (make-instance 'hssg.filesystem:write-string-contents
      :contents (format nil "<!DOCTYPE html>~%~A" contents)
      :path output))))

(defun read-html-lisp (fpath output &optional &key (template #'identity) (initial nil))
  "Read the contents of an HTML document from a Lisp file."
  (declare (type (or string pathname) fpath output))
  (let ((data (funcall (hssg.reader:get-reader "lisp") fpath)))
    (make-instance 'hssg.artifact:html-artifact :data (append data initial)
                                  :output output
                                  :template template)))

(defun normalize (symbol)
  "Convert an atom to its lower-case representation."
  (declare (type atom symbol)
           (optimize (speed 3) (safety 0)))
  (etypecase symbol
    (string symbol)
    (symbol (string-downcase (symbol-name symbol)))
    (atom   (string-downcase (princ-to-string symbol)))))

(defun set-attributes (node attributes)
  "For a given NODE set its attributes, where ATTRIBUTES is an alternating list
  of key-value items."
  (declare (type plump:element node)
           (type list attributes)
           (optimize (speed 3) (safety 0)))
  (loop for (attribute value) on attributes by #'cddr
        when value
        do (setf (plump:attribute node (normalize attribute))
                 (if (eq value t)
                   (normalize attribute)
                   (princ-to-string value)))))

(defun sexp->plump-tree (sexp &optional (parent (plump:make-root)))
  "Convert an s-expression (atom or tree) into a tree of cl-plump nodes. The
  conversion details depend on the type of s-expression:
    - Strings become plain text nodes
    - Symbols are first normalised to strings, then converted
    - Lists become trees with children"
  (declare (type plump:node parent)
           (type (or string symbol list) sexp)
           (optimize (speed 3) (safety 0)))
  (the plump:element
    (etypecase sexp
      (string (plump:make-text-node parent sexp))
      (symbol (sexp->plump-tree (normalize sexp) parent))
      (list
        (let* ((head (first sexp))
               (tail (rest  sexp))
               (tag        (if (atom head) head (first head)))
               (attributes (if (atom head) '()  (rest  head)))
               (node (plump:make-element
                       parent (normalize tag)
                       :children (plump:make-child-array (length tail)))))
          (declare (type list tail))
          (set-attributes node attributes)
          ;; Children will be automatically added to NODE as a side effect
          (loop for child in tail when child do (sexp->plump-tree child node))
          node)))))

(defmacro static-page ((&rest bindings) &body content)
  "Static page DSL macro

  This macro defines a static website with a syntax similar to Lisp's
  let*-expressions. The first argument is a list of (BINDING KEY VALUE)
  triples, the body is the content of the page.
  
  The bindings are used as metadata of the page. The BINDING binds the VALUE
  within the scope of subsequent BINDINGs and the BODY. The KEY is the key
  under which the VALUE will be stored.
  
  The body is a sequence of S-XML elements. All body expressions will be
  grouped inside a list and stored under the key :CONTENT.
  
  The macro expands into an association list. The entries are the metadata
  '(KEY . VALUE) pairs, and a special pair '(:CONTENT . CONTENT), where
  CONTENT is a list of all content expressions.

  Example:

    (static-page ((title  :title \"title\")
                  (author :author \"yo mama\"))
      `(:p \"hello world\")
      `(:p \"by \" ,author))"
  ;; TODO: make it possible to not bind a BINDING, i.e. write (:key val)
  ;; instead of (var :key val) if the binding is not needed.

  (let ((vars (mapcar #'car   bindings))
        (keys (mapcar #'cadr  bindings))
        (vals (mapcar #'caddr bindings)))
    `(let* ,(mapcar (lambda (var val) `(,var ,val)) vars vals)
       (list
         ,@(mapcar (lambda (key var) `(cons ,key ,var)) keys vars)
         (cons :content (list ,@(mapcar (lambda (c) `(,@c)) content)))))))
