;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; verbatim.lisp  Verbatim artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact.directory)

(defmethod hssg.artifact:derive-artifact ((artifact hssg.artifact:directory-artifact))
  (with-slots ((path      hssg.artifact::directory)
               (base-path hssg.artifact::base))
      artifact
    (make-instance 'hssg.filesystem:copy-directory
      :base-path (fad:pathname-as-directory base-path)
      :path      (fad:pathname-as-directory path))))

(defun make-directory-artifact (base directory)
  "Constructor for a verbatim directory artifact which copies an entire
  directory FILE-PATH and its contents recursively from INPUT-DIR to
  OUTPUT-DIR."
  (declare (type (or string pathname) base directory))
  (make-instance 'hssg.artifact:directory-artifact
    :base base :directory directory))
