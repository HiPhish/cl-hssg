;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; reader.lisp  Interface to the various content readers
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.reader)

(defvar *file-readers* '()
  "Association list of file readers; each entry maps a type string to a reader
  function. A reader function is a function which accepts a PATHNAME object and
  returns an association list of metadata.")

(defun get-reader (type)
  "For a given file TYPE return the currently registered file reader function."
  (declare (type string type))
  (let ((entry (assoc type *file-readers* :test #'string=)))
    (if entry
        (cdr entry)
        (error "No file reader registered for type ~A." type))))

(defun register-reader (type reader)
  "Permanently registeres a new READER function for the given TYPE."
  (declare (type string type))
  (push (cons type reader) *file-readers*))

(defun unregister-reader (type)
  (declare (type string type))
  (flet ((matches-type-p (entry)
           (string-equal type (car entry))))
    (setf *file-readers*
          (remove-if #'matches-type-p
                     *file-readers*))))

(defmacro with-readers ((&rest reader-spec) &body body)
  "Evaluate the BODY expressions with temporarily registered readers. Each
  reader specification is a list of to elements of the form (TYPE READER). The
  entire expression evaluates to the evaluation of the last BODY form."
  (flet ((reader-spec->list (spec)
           (destructuring-bind (type reader) spec
             `(cons ,type ,reader))))
    `(let ((*file-readers* (concatenate 'list
                                        (list ,@(mapcar #'reader-spec->list reader-spec))
                                        *file-readers*)))
       ,@body)))
