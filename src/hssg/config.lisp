;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; config.lisp  Mutable global variables for configuring the entire generator
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg)

(defparameter *site-url* "localhost"
  "URL of the entire website (without trailing slash); this parameter needs to
  be rebound when writing the website. Its value will be used whenever we need
  the full URL to the website.

  We should use relative URL paths wherever possible, but sometime it is
  necessary to expose a fully qualified URL, e.g. when providing a share link
  or generating an RSS feed.")

(defparameter *site-language* "en"
  "Language code of the website, can for example be inserted into the 'lang'
  attribute of the '<html>' tag.")
