;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; config.lisp  Mutable global variables for configuring the entire generator
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:hssg.blog.reader.cmark
  (:documentation "Helper package which exposes the CommonMark blog post reader.")
  (:use :cl)
  (:export read-common-mark))
(in-package #:hssg.blog.reader.cmark)

(defmacro string-ecase (keyform &body cases)
  "Like ECASE, except that it uses strings as keys."
  (let ((key (gensym)))
    `(let ((,key ,keyform))
       (cond
         ,@(mapcar #'(lambda (case)
                       `((string-equal ,(car case) ,key) ,@(cdr case)))
                   cases)
         (t (error "String key \"~A\" fell through" ,key))))))

(defun read-common-mark (file-name)
  "Reads the contents of a CommonMark file into a blog post"
  (declare (type pathname file-name))
  (with-open-file (input file-name :direction :input)
    (let ((metadata (loop with line = (read-line input)
                          until (> (ppcre:count-matches "----*" line) 0)
                          if (> (length line) 0)
                            collect line
                          do (setf line (read-line input))))
          (content (cmark:parse-stream input)))
      (cons (cons :content (read-cmark-node content))
            (mapcar #'parse-metadata-line metadata)))))

(setf (gethash "md" hssg.blog:*BLOG-POST-READERS*) #'read-common-mark)

(defun parse-metadata-line (line)
  "Parses a metadata string and returns the resulting metadata pair."
  (destructuring-bind (key value) (ppcre:split ":\\s+" line :limit 2)
    (string-ecase key
      ("title"     (cons :title     value))
      ("author"    (cons :author    value))
      ("published" (cons :published (local-time:parse-timestring value)))
      ("modified"  (cons :modified  (local-time:parse-timestring value)))
      ("category"  (cons :category  value))
      ("tags"      (cons :tags      (ppcre:split ",\\s*" value))))))

(defgeneric read-cmark-node (node)
  (:documentation "Reads a CommonMark node into an SXML expression.")
  (:method ((node cmark:node))
    (error "CommonMark reader not implemented for ~A" (class-of node))))

(defmethod read-cmark-node ((node cmark:document-node))
  (mapcar #'read-cmark-node (cmark:node-children node)))

(defmethod read-cmark-node ((node cmark:block-quote-node))
  `(:blockquote ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:list-node))
  (let ((type (cmark:node-list-type node))
        (start (cmark:node-list-start node)))
    `(,(ecase type
         (:cmark-bullet-list :ul)
         (:cmark-ordered-list (if (and start (not (= start 1)))
                                (list :ol :start start)
                                :ol)))
       ,@(mapcar #'read-cmark-node (cmark:node-children node)))))

(defmethod read-cmark-node ((node cmark:item-node))
  `(:li ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:code-block-node))
  `(:pre
     (:code ,(cmark:node-literal node))))

(defmethod read-cmark-node ((node cmark:paragraph-node))
  `(:p ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:heading-node))
  `(,(ecase (cmark:node-heading-level node)
       (1 :h1)
       (2 :h2)
       (3 :h3)
       (4 :h4)
       (5 :h5)
       (6 :h6)
       (7 :h7))
     ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:thematic-break-node))
  (list :br))

(defmethod read-cmark-node ((node cmark:text-node))
  (cmark:node-literal node))

(defmethod read-cmark-node ((node cmark:softbreak-node))
  "
")

(defmethod read-cmark-node ((node cmark:linebreak-node))
  "
")

(defmethod read-cmark-node ((node cmark:code-node))
  `(:code ,(cmark:node-literal node)))

(defmethod read-cmark-node ((node cmark:emph-node))
  `(:em ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:strong-node))
  `(:strong ,@(mapcar #'read-cmark-node (cmark:node-children node))))

(defmethod read-cmark-node ((node cmark:link-node))
  (let ((url (cmark:node-url node))
        (title (cmark:node-title node)))
    `((:a :href ,url ,@(if (> (length title) 0) `(:title ,title) nil))
        ,@(mapcar #'read-cmark-node (cmark:node-children node)))))

(defmethod read-cmark-node ((node cmark:image-node))
  (let ((url      (cmark:node-url      node))
        (title    (cmark:node-title    node))
        (children (cmark:node-children node)))
    `((:img :src ,url
           ,@(if (> (length title) 0) `(:title ,title) nil)
           ,@(if children `(:alt ,@(mapcar #'read-cmark-node children)) nil)))))

(hssg.reader:register-reader "md" #'read-common-mark)
