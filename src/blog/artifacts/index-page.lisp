;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; index-page.lisp  Implementation of the "index" page artifact
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.index-page)

(defun url-to-breadcrumbs (url)
  "Converts a URL (list of strings) to breadcrums (list of a-lists)."
  (maplist (lambda (items)
             (let ((level (1- (length items))))
               (if (zerop level)
                   `((:title . ,(car items)))
                   `((:title . ,(car items))
                     (:url . ,(format nil "~V@{~A/~:*~}" level ".."))))))
           url))

(defun page->html (page)
  "Transforms a blog index page artifact to an HTML page artifact."
  (with-slots ((url     hssg.blog.artifacts:url)
               (blog    hssg.blog.artifacts:blog)
               (number  hssg.blog.artifacts:number)
               (total   hssg.blog.artifacts:total)
               (posts   hssg.blog.artifacts:posts))
      page
    (make-instance 'hssg:html-artifact
      :data `((:page        . ,number)
              (:pages       . ,total)
              (:blog        . ,blog)
              (:posts       . ,posts)
              (:breadcrumbs . ,(url-to-breadcrumbs url))
              ,@(slot-value blog 'hssg.blog.artifacts:initial))
      :template (hssg:chain-templates
                  'hssg.blog.template:article-index
                  'hssg.blog.template:index
                  'hssg.blog.template:blog-page
                  (slot-value blog 'hssg.blog.artifacts:template))
      :output (apply #'fad:merge-pathnames-as-file
                `(,@(mapcar #'fad:pathname-as-directory url)
                  ,(fad:pathname-as-file "index.html"))))))


(defmethod hssg:derive-artifact ((page index-page-artifact))
  (let ((html-page (page->html page)))
    (hssg:derive-artifact html-page)))
