;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; post.lisp  Implementation of the "blog post" page artifact
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.post)

(defun post->html (post) 
  "Transforms one blog post to an HTML page artifact."
  (declare (type post-artifact post))
  (with-slots ((blog hssg.blog.artifacts:blog)
               (slug hssg.blog.artifacts:slug)
               (title hssg.blog.artifacts:title)
               (content hssg.blog.artifacts:content)
               (category hssg.blog.artifacts:category)
               (tags hssg.blog.artifacts:tags)
               (author hssg.blog.artifacts:author)
               (published hssg.blog.artifacts:published)
               (modified hssg.blog.artifacts:modified)
               (status hssg.blog.artifacts:status)
               (previous hssg.blog.artifacts:previous)
               (next hssg.blog.artifacts:next)
               (metadata hssg.blog.artifacts:metadata))
      post
    (make-instance 'hssg.artifact:html-artifact
      :data `((:blog . ,blog)
              (:post . ((:slug . ,slug)
                        (:title . ,title)
                        (:content . ,content)
                        (:category . ,category)
                        (:tags . ,tags)
                        (:author . ,author)
                        (:published . ,published)
                        (:modified . ,modified)
                        (:status . ,status)
                        ,@metadata))
              (:prev . ,previous)
              (:next . ,next)
              ,@(slot-value blog 'hssg.blog.artifacts:initial))
      :template (hssg:chain-templates
                  'hssg.blog.template:post
                  'hssg.blog.template:blog-page
                  (slot-value blog 'hssg.blog.artifacts:template))
      :output (fad:merge-pathnames-as-file
                (hssg.blog.util:date->pathname published)
                (fad:pathname-as-directory slug)
                (fad:pathname-as-file "index.html")))))

(defmethod hssg:derive-artifact ((post post-artifact))
  (let ((html-page (post->html post)))
    (hssg:derive-artifact html-page)))
