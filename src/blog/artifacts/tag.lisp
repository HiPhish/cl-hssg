;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; tag.lisp  Implementation of the "tag" page artifact
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.tag)

(defun fetch-tag (tags name)
  "Retrieve the tag object with a given NAME from a BLOG artifact; if there is
  no tag object it is created and registered first."
  (declare (type hssg.blog.artifacts:tags-artifact tags)
           (type string name))
  (with-slots ((items hssg.blog.artifacts:items)
               (blog  hssg.blog.artifacts:blog))
      tags
    (or (find name items :test #'string= :key #'hssg.blog.artifacts:tag-name)
        (let ((result (make-instance 'tag-artifact :name name :blog blog)))
          (push result items)
          result))))

(defun add-post (tags name post)
  "Adds a single POST to the TAGS collection under the key NAME."
  (with-slots ((posts hssg.blog.artifacts:posts))
      (fetch-tag tags name)
    (push post posts)))

(defun tags->html (items blog) 
  "Reduce them ITEMS of a TAGS-ARTIFACT instance to an HTML page artifact."
  (make-instance 'hssg:html-artifact
    :data `((:tags . ,items)
            (:blog . ,blog)
            ,@(slot-value blog 'hssg.blog.artifacts:initial))
    :template (hssg:chain-templates
                'hssg.blog.template:tags
                'hssg.blog.template:blog-page
                (slot-value blog 'hssg.blog.artifacts:template))
    :output (fad:merge-pathnames-as-file
              (fad:pathname-as-directory (hssg.blog.i18n:localise :url-tags))
              (fad:pathname-as-file "index.html"))))


(defmethod hssg:derive-artifact ((tag tag-artifact))
  "Compound instruction, one instruction per index page."
  (with-slots ((blog  hssg.blog.artifacts:blog)
               (name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      tag
    (make-instance 'hssg:compound-instruction
      :instructions (mapcar
                      #'hssg:derive-artifact
                      ;; FIXME: use localised name instead of "tags"
                      (collect-index-pages posts blog (list "tags" name))))))

(defmethod hssg:derive-artifact ((tags tags-artifact))
  "Compound instruction: one instructions for the whole tags page and one
  instruction per individual tag (which itself is a compound instruction)."
  (with-slots ((items hssg.blog.artifacts:items)
               (blog  hssg.blog.artifacts:blog))
      tags
    (let ((tags-page   (hssg:derive-artifact (tags->html items blog)))
          (tag-indices (mapcar #'hssg:derive-artifact items)))
      (make-instance 'hssg:compound-instruction
        :instructions (cons tags-page tag-indices)))))
