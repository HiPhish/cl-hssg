;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; config.lisp  Mutable global variables for configuring the entire generator
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifacts)

(defparameter *posts-per-page* 10
  "How many blog posts to display per index page")

(defclass blog-artifact ()
  ((title :initarg :title :type string
          :documentation "Human-readable title of the blog.")
   (description :initarg :description :initform "" :type string
                :documentation "Human-readable description of the blog")
   (posts :initform (list) :type list
          :documentation "List of blog posts ordered from most recent to oldest.")
   (categories :initform (make-instance 'categories-artifact) :type categories-artifact
               :documentation "Collection of categories of the blog")
   (tags :initform (make-instance 'tags-artifact) :type tags-artifact
         :documentation "Collection of tags of the blog")
   (authors :initform (make-hash-table :test 'equal) :type hash-table
            :documentation "Maps an author's name to an unordered set of posts.")
   (periods :initform (list) :type list
            :documentation "Year periods of the blog.")
   (top :initarg :top :initform "" :type string
        :documentation "Identifier to use as the top of the breadcrumbs")
   (url :initarg :url :type list
        :documentation "URL of the blog as a list of URL path strings, relative
  to the root of the site.")
   (template :initarg :template :initform #'hssg:identity-template :type hssg:template
             :documentation "Base template to apply to all pages")
   (initial :initarg :initial :initform '() :type list
            :documentation "Initial metadata for all pages.")
   (static :initform (hssg:make-compound-artifact)
           :documentation "Static files which will be added to the blog verbatim."))
  (:documentation "Artifact which represents an entire blog."))

(defclass post-artifact ()
  ((blog :initarg :blog :type blog-artifact
         :documentation "Blog this post belongs to.")
   (slug :initarg :slug :type string
         :documentation "Short string identifier, used for the URL; not necessarily unique.")
   (title :initarg :title :type string
          :documentation "Full title of the post")
   (content :initform '() :initarg :content :type list
            :documentation "Content of the post as SXML.")
   (category :initform hssg.blog.i18n:*default-category* :initarg :category :type string
             :documentation "Name of the blog post category.")
   (tags :initform '() :initarg :tags :type list
         :documentation "Names of the blog post tags")
   (author :initform nil :initarg :author :type (or null string)
           :documentation "Name of author of the blog post.")
   (published :initarg :published :type local-time:timestamp)
   (modified :initarg :modified :type (or null local-time:timestamp))
   (status :initform :published :initarg :status :type (member :published :draft)
           :documentation "Status of the post, either :PUBLISHED (default) or :DRAFT.")
   (previous :initform nil :initarg :prev :type (or null post-artifact)
             :documentation "Previous blog post")
   (next :initform nil :initarg :next :type (or null post-artifact)
         :documentation "Next blog post")
   (metadata :initform '() :initarg :metadata :type list
             :documentation "Association list of extra metadata for the post page, will be
  passed to the HTML template."))
  (:documentation "Artifact which represents a single blog post."))

(defclass categories-artifact ()
  ((items :initform '() :type list
          :documentation "List of categories recorded this far.")
   (blog :initarg :blog :type blog-artifact
         :documentation "Blog these categories belong to."))
  (:documentation "Container of all individual category artifacts."))

(defclass category-artifact ()
  ((blog :initarg :blog :type blog-artifact
         :documentation "Blog this category belongs to.")
   (name :initarg :name :reader category-name :type string
         :documentation "Name of the category")
   (posts :initarg :posts :initform '() :type list
          :documentation "Posts which belong to this category"))
  (:documentation "A category of blog posts"))

(defclass tags-artifact ()
  ((items :initform '() :type list
          :documentation "List of tag artifacts recorded this far.")
   (blog :initarg :blog :type blog-artifact
         :documentation "Blog these categories belong to."))
  (:documentation "Container of all individual tag artifacts."))

(defclass tag-artifact ()
  ((blog :initarg :blog :type blog-artifact
         :documentation "Blog this tag belongs to.")
   (name :initarg :name :reader tag-name :type string
         :documentation "Name of the tag")
   (posts :initarg :posts :initform '() :type list
          :documentation "Posts which belong to this tag"))
  (:documentation "A tag of blog posts"))

(defclass index-page-artifact ()
  ((url :initarg :url :type string
        :documentation "Under which URL to save the index page (excluding index number).")
   (posts :initarg :posts :type list
          :documentation "Posts to display on this page")
   (number :initarg :number :type (integer 1)
           :documentation "Number of this page among all pages")
   (total :initform 1 :initarg :total :type (integer 1)
          :documentation "How many pages there are in total.")
   (previous :initarg :prev :initform nil :type (or null index-page-artifact)
             :documentation "Previous page, if any")
   (next :initarg :next :initform nil :type (or null index-page-artifact)
         :documentation "Next page, if any")
   (blog :initarg :blog :type blog-artifact
         :documentation "The blog instance this index belongs to."))
  (:documentation "A single page listing a number of posts with their preview."))

(defclass archive-page-artifact ()
  ((periods :initarg :periods :type list
            :documentation "Association-tree of the blog's post periods.")
   (blog :initarg :blog :type blog-artifact
         :documentation "Blog this archive belongs to."))
  (:documentation "Archive page which lists all posts grouped by their period."))

(defclass rss-feed-artifact ()
  ((posts :initarg :posts)
   (blog :initarg :blog :type blog-artifact
         :documentation "Blog this RSS feed belongs to."))
  (:documentation "RSS feed of blog posts."))
