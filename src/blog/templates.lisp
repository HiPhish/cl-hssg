;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; templates.lisp  Templates used by blog pages
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template)

(hssg:deftemplate blog-page (breadcrumbs content blog css)
  "The blog template is a wrapper for all blog-related pages (individual
  articles, archives, categories,...). A blog page itself may be further
  processed by other templates.

  Base template for all blog-related pages, the result is to be spliced into a
  page.

  Required metadata:

   - breadcrumbs List of a-lists; each a-list is a breadcrumb item
   - content     The content to display in the middle of the page, e.g. an
                 entire blog post, a listing of categories or a listing of blog
                 post previews.
   - blog        Metadata about the current blog to process.
   - categories  Association list of categories. The key is the name of the
                 category (a string), the value is a list of posts in that
                 category.
   - tags        the same as categories, except for tags."
  (:css
    (cons "/css/blog.css" css))
  (:content
    (let ((url (slot-value blog 'hssg.blog.artifacts:url)))
      `(((:div :class "blog")
          ((:nav :class "breadcrumbs" :aria-label "Breadcrumbs")
            (:ol
              ,@(mapcar #'hssg.blog.template.blog:breadcrumb->sxml breadcrumbs)))
          ,@content
          ;; Left pillar, article navigation
          ((:nav :class "blog-navigation" :aria-label "Blog navigation")
            ;; This navigator contains links to the various archive types.
            (:aside
              (:span ,(format nil "~A:" (hssg.blog.i18n:localise :subscribe)))
              " "
              ((:a :href ,(format nil "/~{~A/~}rss.xml" url) :type "application/rss+xml")
                "RSS"))
            (:nav
              (:h1
                ((:a :href ,(format nil "/~{~A/~}~A/" url (hssg.blog.i18n:localise :url-archive)))
                  ,(hssg.blog.i18n:localise :archive)))
              (:ul
                ; For each year display a year link to that year's archive. If the
                ; year is the year of the current post display a sub-list for that year
                ,@(mapcar (lambda (period) (funcall #'hssg.blog.template.blog:period->sxml period url))
                          (slot-value blog 'hssg.blog.artifacts:periods))))
            (:nav
              (:h1
                ((:a :href ,(format nil "/~{~A/~}~A/" url (hssg.blog.i18n:localise :url-categories)))
                  ,(hssg.blog.i18n:localise :categories)))
              (:ul
                ,@(let ((categories (slot-value (slot-value blog 'hssg.blog.artifacts:categories)
                                                'hssg.blog.artifacts:items)))
                    (mapcar #'hssg.blog.template.blog:blog-category->sxml
                            categories
                            (make-sequence 'list (length categories) :initial-element url)))))
            (:nav
              (:h1
                ((:a :href ,(format nil "/~{~A/~}~A/" url (hssg.blog.i18n:localise :url-tags)))
                  ,(hssg.blog.i18n:localise :tags)))
              (:ul
                 ,@(let ((tags (slot-value (slot-value blog 'hssg.blog.artifacts:tags) 'hssg.blog.artifacts:items)))
                     (mapcar #'hssg.blog.template.blog:blog-tag->sxml
                             tags
                             (make-sequence 'list (length tags) :initial-element url)))))))))))

(hssg:deftemplate archive (periods blog)
  (:title (hssg.blog.i18n:localise :archive))
  (:url (hssg.blog.i18n:localise :url-archive))
  (:breadcrumbs `(((:title . ,(slot-value blog 'hssg.blog.artifacts:top))
                   (:url   . "../"))
                  ((:title . ,(hssg.blog.i18n:localise :archive)))))
  (:content
    `(((:main :id "archive")
       (:ul
         ,@(mapcar #'hssg.blog.template.archive:archive-year->sxml periods))))))

(hssg:deftemplate categories (blog categories)
  "Concrete template for all categories in the blog. Does not generate any
  actual content, only sets up the metadata for the next step in the pipeline.

  Required metadata:
    - blog        Information about the blog
    - categories  List of association list of categories"
  (:title (hssg.blog.i18n:localise :categories))
  (:url   (hssg.blog.i18n:localise :url-categories))
  (:breadcrumbs
    `(((:title . ,(slot-value blog 'hssg.blog.artifacts:top))
       (:url   . "../"))
      ((:title . ,(hssg.blog.i18n:localise :categories)))))
  (:content
    `((:ul
        ,@(mapcar #'hssg.blog.template.category&tag:category->sxml categories)))))

(hssg:deftemplate tags (blog tags)
  "Concrete template for all tags in the blog. Does not generate any actual
  content, only sets up the metadata for the next step in the pipeline.

  Required metadata:
    - blog  Information about the blog
    - tags  List of association list of tags"
  (:title (hssg.blog.i18n:localise :tags))
  (:url   (hssg.blog.i18n:localise :url-tags))
  (:breadcrumbs
    `(((:title . ,(slot-value blog 'hssg.blog.artifacts:top))
       (:url   . "../"))
      ((:title . ,(hssg.blog.i18n:localise :tags)))))
  (:content
    `((:ul
        ,@(mapcar #'hssg.blog.template.category&tag:tag->sxml tags)))))

(hssg:deftemplate article-index (pages page posts)
  "Template for an article index; article indices list articles for a give
  group (e.g. a category). The content is a sequence of articles, usually
  showing their title with a link to the full article, and a preview of the
  full article.

  Required metadata:
    - pages    Number of pages in the paginator
    - page     Number of the current pagination page
    - posts    List of posts contained inside this index page."
  (:content `(((:main :class "blogpost-listing")
                (:ul
                  ,@(mapcar #'hssg.blog.template.index:post->preview posts)))
             ,(if (> pages 1)
                `((:nav :class "paginator")
                     (:ul 
                       ,@(funcall #'hssg.blog.template.index:paginator page pages)))
                '()))))

(hssg:deftemplate index (blog breadcrumbs)
  "Template for the blog main index, to be spliced into the blog template.

  The main index of the blog is what the user first sees when visiting the
  blog.  It displays all articles (paginated of course) from newest to oldest."
  (:breadcrumbs
     `(((:title . ,(slot-value blog 'hssg.blog.artifacts:top))
        ;; Number of levels must be based on the number of previous elements
        (:url . ,(format nil "~V@{~A/~:*~}" (length breadcrumbs) "..")))
       ,@breadcrumbs)))


(hssg:deftemplate post (blog post prev next)
  "Template for an individual blog post. Required metadata:
  
    - blog  Information on the blog
    - post  The current post to display
  
  Optional metadata:
  
    - prev  Previous post
    - next  Next post"
  
  (:css
    (hssg:let-metadata ((css :css)) post
      css))
  (:periods
    (with-slots ((periods hssg.blog.artifacts:periods ))
        blog
      periods))
  (:breadcrumbs
    (hssg:let-metadata ((date :published)
                        (slug :slug))
        post
      `(((:title . ,(slot-value blog 'hssg.blog.artifacts:top))
         (:url   . "../../../../"))
        ((:title . ,(hssg.blog.util:date->year date))
         (:url   . "../../../"))
        ((:title . ,(hssg.blog.util:date->month date))
         (:url   . "../../"))
        ((:title . ,(hssg.blog.util:date->day date)))
        ((:title . ,slug)))))
  (:content
    (hssg:let-metadata ((status   :status :published)
                        (title    :title)
                        (date     :published)
                        (modified :modified)
                        (category :category)
                        (tags     :tags '())
                        (author   :author)
                        (content  :content))
        post
      `(((:main :class "blogpost")
          (:article
            (:h1
              ((:a :href  "." :title ,(format nil "~A ~A" (hssg.blog.i18n:localise :permalink) title) :rel "bookmark")
                ,title))
            ((:header :class "blog-post-header")
               ;; Draft status notification
               ,(if (eq status :draft)
                 `((:aside :class "alert alert-info" :role "alert")
                    (:strong (format nil "~A:" ,(hssg.blog.i18n:localise :draft)))
                    ,@(hssg.blog.i18n:localise :draft-msg))
                 nil)
               ;; Date of the blog post
               ((:p :class "blog-post-published")
                 ,(format nil "~A: " (hssg.blog.i18n:localise :published))
                 ((:time :datetime ,(hssg.blog.util:date->string date))
                    ,(hssg.blog.util:date->string date)))
               ,(if modified
                 `((:p :class "blog-post-modified")
                    ,(format nil "~A: " (hssg.blog.i18n:localise :modified))
                    ((:time :datetime ,(hssg.blog.util:date->string modified))
                      ,(hssg.blog.util:date->string modified)))
                 nil)
               ;; Category of the blog post, if any
               ,(if category
                    `((:p :class "blog-post-category")
                       ,(format nil "~A: " (hssg.blog.i18n:localise :categories))
                       ((:a :href ,(format nil "../../../../~A/~A/" (hssg.blog.i18n:localise :url-categories)
                                                                    category))
                          ,category))
                    nil)
               ;; Tags of the blog post, if any
               ,(if tags
                    `((:p :class "blog-post-tags")
                       ,(format nil "~A: " (hssg.blog.i18n:localise :tags))
                       ,@(hssg.blog.util:intersperse (mapcar #'hssg.blog.template.post:post-tag->sxml tags) ", "))
                    nil)
               ;; Author of the blog post, if any
               ,(if author nil nil)  ; Just a stub
               )
             ,@content)
        ,(if (or prev next)
           (hssg.blog.template.post:pager->sxml prev next)
           nil))))))
