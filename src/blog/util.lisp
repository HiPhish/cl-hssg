;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; util.lisp  Various utilities used throughout the blog plugin
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.util)

(defun range (from to)
  "Produce a list of integers FROM to TO (both inclusive)"
  (loop for i from from to to collecting i))

(defun break-list (list n)
  "Breaks a LIST in two where the first N elements are in a new list and the
  remainder is the CDR of the original LIST."
  (do ((lhs '())
       (rhs list)
       (i n (decf i)))
      ((or (zerop i) (null rhs))
       (values (nreverse lhs) rhs))
    (push (car rhs) lhs)
    (pop rhs)))

(defun date-components (date)
  (values (local-time:timestamp-year date)
          (local-time:timestamp-month date)
          (local-time:timestamp-day date)))

(defun date-from-numbers (year month day)
  "Creates a date object with given integer YEAR, MONTH and DAY."
  (local-time:encode-timestamp 0 0 0 0 day month year :timezone local-time:+utc-zone+))

(defun date->year (date)
  "Format the year component a timestamp object to a string."
  (local-time:format-timestring nil date :format '(:year)))

(defun date->month (date)
  "Format the month component a timestamp object to a string."
  (multiple-value-bind (year month day) (date-components date)
    (declare (ignore year day))
    (format nil "~2,'0D" month)))

(defun date->day (date)
  "Format a day component of timestamp object to a string"
  (multiple-value-bind (year month day) (date-components date)
    (declare (ignore year month))
    (format nil "~2,'0D" day)))

(defun date->string (date)
  "Format a timestamp object to a string of the form \"year-month-day\"."
  (local-time:format-rfc3339-timestring nil date :omit-time-part t))

(defun date->url (date)
  "Format a timestamp object to a URL path string of the form
  \"year/month/day\"."
  (multiple-value-bind (year month day) (date-components date)
    (format nil "~D/~2,'0D/~2,'0D" year month day)))

(defun date->pathname (date)
  "Format a timestamp object to a directory pathname of the form
  #p\"year/month/day\"."
  (multiple-value-bind (year month day) (date-components date)
    (let ((year (format nil "~D" year))
          (month (format nil "~2,'0D" month))
          (day (format nil "~2,'0D" day)))
      (apply #'fad:merge-pathnames-as-directory
             (mapcar #'fad:pathname-as-directory (list year month day))))))

(defun intersperse (list item)
  "For a given LIST return a new list with ITEM interspersed between each list
  item."
  (let ((result '()))
    (dolist (current list (nreverse (cdr result)))
      (push current result)
      (push item result))))

(defun directory-contents (path &key (test #'fad:directory-pathname-p))
  "Contents of a directory, sorted alphabetically in ascending order."
  (declare (type pathname path)
           (type (function (pathname) boolean) test))
  (sort (remove-if-not test (fad:list-directory path))
        #'string<= :key #'namestring))

