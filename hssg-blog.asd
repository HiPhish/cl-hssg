;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; hssg-blog.asd  Hackable Static Site Generator blog extension system
;;;; definition Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG-BLOG.
;;;;
;;;; CL-HSSG-BLOG is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG-BLOG is distributed in the hope that it will be useful, but
;;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
;;;; License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG-BLOG  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:hssg-blog
  :description "Blog extension to HSSG"
  :author "HiPhish <hiphish@posteo.de>"
  :license  "AGPL-3.0-or-later"
  :version "0.0.0"
  :depends-on ("hssg" "cmark" "cl-fad")
  :serial t
  :components ((:module "src"
                :components ((:module "blog"
                              :components ((:file "package")
                                           (:file "config")
                                           (:file "util")
                                           (:file "i18n")
                                           (:module "periods"
                                            :components ((:file "classes")
                                                         (:file "protocol")))
                                           (:module "readers"
                                            :components ((:file "readers")
                                                         (:file "lisp")))
                                           (:module "artifacts"
                                            :components ((:file "classes")
                                                         (:file "util")
                                                         (:file "archive")
                                                         (:file "blog")
                                                         (:file "category")
                                                         (:file "index-page")
                                                         (:file "post")
                                                         (:file "rss-feed")
                                                         (:file "tag")))
                                           (:module "template"
                                            :components ((:file "archive")
                                                         (:file "blog")
                                                         (:file "category-tag")
                                                         (:file "index")
                                                         (:file "post")))
                                           (:file "templates")
                                           (:file "facade"))))))
  :in-order-to ((test-op (test-op "hssg-blog/test"))))

(asdf:defsystem #:hssg-blog/test
  :description "Tests for HSSG-BLOG"
  :author "HiPhish <hiphish@posteo.de>"
  :license  "AGPL-3.0-or-later"
  :version "0.0.0"
  :depends-on ("hssg-blog" "clunit2")
  :serial t
  :perform (test-op (o s)
             (symbol-call :hssg-blog/test :test-all))
  :components ((:module "test"
                :components ((:file "mocking")
                             (:module "blog"
                              :components ((:file "main")
                                           (:module "artifacts"
                                            :components ((:file "archive")))))))) )
