;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; xml.lisp  XML artifact tests
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>
(defpackage #:hssg/test/artifact/xml
  (:use #:cl)
  (:import-from #:clunit #:defsuite #:deffixture #:deftest #:assert-equal #:assert-true)
  (:import-from #:hssg.artifact #:xml-artifact #:derive-artifact))
(in-package #:hssg/test/artifact/xml)

(clunit:defsuite hssg.artifact.xml (hssg/test:hssg))


;;; ---------------------------------------------------------------------------
;;; NOTE: we do not test the produced XML, only that some XML was produced in
;;; the first place.

(deftest derive-xml-artifact (hssg.artifact.xml)
  (let* ((artifact (make-instance 'hssg:xml-artifact :data '((:foo . "bar")) :output #p"blog/rss.xml"))
         (instruction (derive-artifact artifact)))
    (assert-true (typep instruction 'hssg:write-string-contents))
    (with-slots ((contents hssg.filesystem::contents)
                 (path     hssg.filesystem::path))
        instruction
      (assert-equal "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foo></foo>" contents)
      (assert-equal #p"blog/rss.xml" path))))
