;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; compound.lisp  Compound artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.(in-package #:hssg.artifact)
(defpackage #:hssg/test/reader
  (:use #:cl))
(in-package #:hssg/test/reader)

(defun dummy-reader (pathname)
  "A dummy reader which does nothing useful."
  (declare (ignore pathname))
  '())

;; "Tests for the reader interface"
(clunit:defsuite hssg.reader (hssg/test:hssg))

(clunit:deffixture hssg.reader (@body)
  "Installs the dummy \"foo\" reader."
  (let ((hssg.reader::*file-readers* `(("foo" . ,#'dummy-reader))))
    @body))

(clunit:deftest retrieve-reader (hssg.reader)
  "A registered reader can be retrieved by its type"
  (clunit:assert-true (eq #'dummy-reader (hssg.reader:get-reader "foo"))))

(clunit:deftest retrieve-missing-reader (hssg.reader)
  "Retrieving a reader that has not been registered signals a condition"
  (clunit:assert-condition error
    (hssg.reader:get-reader "bar")))

(clunit:deftest add-reader (hssg.reader)
  "A new reader can be added by file type"
  (hssg.reader:register-reader "bar" #'dummy-reader)
  (let ((reader (hssg.reader:get-reader "bar")))
    (clunit:assert-eq #'dummy-reader reader reader)))

(clunit:deftest remove-reader (hssg.reader)
  "An added reader can be removed"
  (hssg.reader:register-reader "bar" #'dummy-reader)
  (hssg.reader:unregister-reader "bar")
  (clunit:assert-condition error (hssg.reader:get-reader "bar")))

(clunit:deftest temporary-reader (hssg.reader)
  "Register a reader for the duration of the body expressions only"
  (hssg.reader:with-readers (("bar" #'dummy-reader))
    (clunit:assert-eq #'dummy-reader (hssg.reader:get-reader "bar")))
  (clunit:assert-condition error (hssg.reader:get-reader "bar")))
