.. default-role:: code

This directory contains a number of sample files and directories for testing
functions which need to operate on actual on-disc files.

`metadata.lisp`
   A sample Lisp file such as they are used to create HTML pages.

`directory-tree`
   A directory containing files and nested directories, used for functions
   which operate recursively on a file tree.
